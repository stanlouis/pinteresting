# Setting The Root Path with Routes

You can direct people toward a specific page by setting a route to point to that page.

Browse Source Code

# 1. Show the Homepage of Your App
config/routes.rb

Replace the line

	get "pages/home"

...with the following:

	root "pages#home"

#Creating More Pages
Most likely, you're going to need more pages besides just a "Home page." (For example, an "About" page or a "Contact Us" page.) In order to do that, you'll have to add a new view and set a route for it.

Browse Source Code

##1. Add a new action in your controller
app/controllers/pages_controller.rb


	def about
	end

##2. Create the HTML in your view
app/views/pages/about.html.erb

	<h1>About US</h1>
	<p>We are working on our One Month Rails Pinteresting app</p>

##3. Add your route
config/routes.rb

	get "about" => "pages#about"

#What is Embedded Ruby?

Embedded Ruby (erb) is like magic! You'll be able to add dynamic content to your HTML pages.

Browse Source Code

## 1. Change your HTML link to an embedded Ruby link
app/views/pages/home.html.erb

	<%= link_to "here", "#" %>
	
## 2. Review of this lesson
In HTML, a link looks like this

	<a href="#">here</a>
	
These are embedded Ruby tags

	<%=  %>
	
In Ruby on Rails a link will look like this

	<%= link_to "here", "#" %>`

#Creating Navigation Links

Make it a little bit easier for users to move around your site with navigation links. In Rails, layouts make it really easy to create something (like a navigation bar) and have it show up on every page in your app.

Browse Source Code

## Add Navigation Links
apps/views/layouts/application.html.erb

# Installing the Bootstrap Gem

### Bootstrap is a front-end framework that every beginner should use. It's really easy to install with a simple gem.

Browse Source Code

Resources

Bootstrap 3 [GitHub Gem]: where you go to install the gem
Bootstrap 3 [Main Site Documentation]: where you go after installation to look at documentation on using Bootstrap styles and components
rubygems.org: the official Ruby Gems site
Note: If you are on windows you need to keep "gem 'tzinfo-data', platforms: [:mingw, :mswin]" in the gemfile.

## 1. Add the Bootstrap gem
/Gemfile

	gem 'bootstrap-sass'
	
## 2. Always bundle install to install a new gem
**terminal**

	$ bundle install`

## 3. Understand the Application.css file
app/assets/stylesheets/application.css

Application.css takes all the other files in your /stylesheets directory and combines them for when you run your app.

## 4. Create a new SCSS file
What is SCSS? SCSS is a precompiler for CSS a.k.a. it helps you write CSS quicker. (This will make more sense soon).

app/assets/stylesheets/bootstrap_and_customization.css.scss

	@import 'bootstrap';

## 5. Restart your server
You'll need to restart your server whenever you add a new gem.

**terminal**

CONTROL + C 

	$ rails server

# Add Bootstrap Elements To Pages

Bootstrap comes pre-built with things like buttons, navigation bars, and icons that you can use to make your web app look really nice, really fast.

Browse Source Code

## 1. Add a container to your app!
views/layouts/application.html.erb


	<%= link_to "Home", root_path %>
	     <%= link_to "About", about_path %>
	     <div class="container">
	          <%= yield %>
	     </div>

## 2. Create a _header.html.erb partial
Create the file app/views/layouts/_header.html.erb

## 3. Create link to partial
app/views/layouts/application.html.erb

	<%= render 'layouts/header' %>
	
## 4. Add the nav bar
app/views/layouts/_header.html.erb


	<nav class="navbar navbar-default" role="navigation">
	  <!-- Brand and toggle get grouped for better mobile display -->
	  <div class="navbar-header">
	    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
	      <span class="sr-only">Toggle navigation</span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	      <span class="icon-bar"></span>
	    </button>
	    <a class="navbar-brand" href="#">Pinteresting</a>
	</div>


  <!-- Collect the nav links, forms, and other content for toggling -->

	<div class="collapse navbar-collapse navbar-ex1-collapse">
	    <ul class="nav navbar-nav navbar-right">
	      <li><%= link_to "Home", root_path %></li>
	      <li><%= link_to "About", about_path %></li>
	    </ul>
	  </div><!-- /.navbar-collapse -->
	</nav>


## 5. Require Bootstrap's JavaScript
app/assets/javascripts/application.js



	//= require jquery
	//= require jquery_ujs
	//= require bootstrap-sprockets
	//= require turbolinks
	//= require_tree


## 6. Add viewport
views/layouts/application.html.erb

	<meta name="viewport" content="width=device-width, initial-scale=1.0">


## 7. Add the jumbotron to the Home page
views/pages/home.html.erb


	<div class="jumbotron">
		<h1>Welcome to my app!</h1>
		Click here to <%= link_to "Sign Up", "#", class: "btn btn-primary" %>
	</div>


# Customizing Bootstrap

## Bootstrap can be customized through Sass variables.

Resources

- http://getbootstrap.com/customize/#variables-basics
- http://flatuicolors.com/
- http://startbootstrap.com/

##1. As an example, update the nav bar
app/views/layouts/_header.html.erb

`<nav class="navbar navbar-inverse navbar-default" role="navigation">`

##2. Add custom styles
app/assets/stylessheets/bootstrap_and_customization.css.scss

	$body-bg: #ecf0f1;
	$navbar-inverse-bg: #27ae60;
	$navbar-inverse-link-color: white;
	$brand-primary: #f39c12;
	$jumbotron-bg: #bdc3c7;
	@import 'bootstrap-sprockets';
	@import 'bootstrap';

#Design Improvement

## 1. Style updates
app/assets/stylesheets/bootstrap_and_customization.css.scss

	@import url(http://fonts.googleapis.com/css?family=Lato:400,700);


	$body-bg:                          #ecf0f1;
	$font-family-sans-serif:           'Lato', 'Helvetica Neue', Helvetica, Arial, sans-serif;
	$navbar-height:                    45px;
	$navbar-default-bg:                white;
	$navbar-default-brand-color:       #c0392b;
	$brand-primary:                    #c0392b;
	$jumbotron-bg:                     white;


@import 'bootstrap-sprockets';
@import 'bootstrap';

.center {
     text-align: center;
}

.navbar-brand {
     font-weight: bold;
}
##2. Update nav bar with a container
apps/views/layouts/_header.html.erb

```
<nav class="navbar navbar-static-top navbar-default" role="navigation">
  <div class="container">
...
</div><!-- /.container -->
</nav>
```
##3. Install Package Control for Sublime Text 2
Installation Instructions Remember to restart Sublime Text
Command Shift P
Package Control:Install Package
CoffeeScript
##4. Add a "center class" and "log in" button
/app/views/pages/home.html.erb

```
<div class="jumbotron center">
     <h1>Welcome to my app!</h1>
     <p>
       <%= link_to "Log in", "#", class: "btn btn-default btn-lg" %>
       <%= link_to "Sign up", "#", class: "btn btn-primary btn-lg" %>
  </p>    
</div>
```
##5. Change the main header link from HTML to Ruby
apps/views/layouts/_header.html.erb

`<%= link_to "Pinteresting", root_path, class: "navbar-brand" %>`

# Going Online With Heroku

How do you move your app live so that others can see it? With a great service called Heroku that lets your deploy your app to the web in seconds.

Browse Source Code

## 1. Signup for a Heroku account
https://www.heroku.com/
## 2. Download the Heroku toolbelt
https://toolbelt.heroku.com/

(if that doesn't work try https://toolbelt.herokuapp.com)

## 3. Login to Heroku
**terminal**

```
$heroku login
Email: (enter your email)
Password (enter your password - it will show blank and that's fine)
## 4. Add Heroku keys
```
**terminal**

```
$ heroku keys:add
$ heroku create #creates a new URL for your app
```
## 5. Add new gems and groups for Heroku
/gemfile

```
group :development, :test do
     gem 'sqlite3'
end
```

```
group :production do
     gem 'pg'
     gem 'rails_12factor'
end
```
Note: After adding a production group to your gemfile you should switch to using
`"bundle install --without production"`

## 6. After installing new gems, what will you always do?
**terminal**

`$ bundle install --without production`
## 7. Do the Git Dance
**terminal**

```
$ git add --all
$ git commit -m "bundle install, all ready to push to heroku"
$ git push origin master
```
## 8. Push to Heroku
**terminal**

```
$ git push heroku master #10:54
$ heroku open #13:30
$ heroku rename omr-pinteresting #Replace "omr-pinteresting" with your own name.
```
# Installing Devise So We Can Add users

##Devise is one of the most popular user authentication gems for Rails.

Browse Source Code

Resources

[Devise Documentation][ef039dc3]
[RubyGems][0179f08a]
##1. Make sure you are using the correct version of Ruby in your Gemfile
**terminal**

  [d348be9e]: https://github.com/plataformatec/devise "Devise Documentation"
  [0179f08a]: http://rubygems.org "RubyGems"
  [ef039dc3]: https://github.com/plataformatec/devise "Devise Documentation"

$ ruby -v
Should return Ruby 2.1.2 or higher - use whatever it returns in your Gemfile.

/Gemfile

source 'https://rubygems.org'
ruby '2.1.2'
##2. Add the Devise gem
/Gemfile

`gem 'devise'`
**terminal**

```
$ rvm rvmrc warning ignore allGemfiles #ignores the RVM warning (2:50)
$ bundle install
```
##3. Install Devise
`$ rails generate devise:install`
##4. Devise secret key
Pinterest/config/initializers/devise.rb

If devise asks or says it cannot find a secret key, copy and paste the key the terminal gives you and place it in your devise.rb like so:

**terminal**

```
Devise.setup do |config|
# The secret key used by Devise. Devise uses this key to generate
# random tokens. Changing this key will render invalid all existing
# confirmation, reset password and unlock tokens in the database.
  config.secret_key = "Enter key here if it doesn't exist"
# -
# -
# etc
end

```
#Setting Up Devise

Here I show you how to set up Devise with flash messages and views.

##1. Default URLs

config/environments/development.rb

`config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }`

config/environments/production.rb

`config.action_mailer.default_url_options = { host: 'http://pinteresting-commits.herokuapp.com/' }`
##2. Set "Home" route
We've already set our home route. Check it out yourself:
config/routes.rb

`root "pages#home"`
##3. Flash message
Flash messages are the messages on Websites that say "Thanks for log in" or "Thanks for signing up"
app/views/layouts/application.html.erb

```
<% flash.each do |name, msg| %>
     <%= content_tag(:div, msg, class: "alert alert-#{name}") %>
<% end %>
```
##4. Set precompile to "false"
config/application.rb

`config.assets.initialize_on_precompile = false`
##5. Install the Devise views
terminal

`$ rails g devise:views`

#Generating Users with Devise

##Here I show how to set up a User model with Devise and migrate those changes to your database.

Browse Source Code

## 1. Generate a User model
	$ rails generate devise user
This line above creates a User model for us and a new file: app/models/user.rb

## Q: Did you make a mistake generating a model?

	$ rails destroy devise user #deletes the generate command in case you made a mistake
### The model interacts with our database

Go to db/migration and see the files there should be something like db/migration/20130922022322_devise_create_users.rb #the number is the date you create it

* Migrate your database
	$ rake db:migrate
This command takes the migration file and runs it, so that it generates tables in your database

* Restart your server
	
CONTROL + C to Restart the Server
	
	$ rails server # to start the server again
	
### You'll need to restart your application each time you install a gem or each time you run
 `$rake db:migrate`

 # New User Signup and Signin

 ## Now you can add links for the sign up and sign in pages to your application views

 Browse Source Code

 ### Find all your paths

	$ rake routes
 ###lists all the paths available to your application. You'll be able to add more along the way.

 ## 1. Update your Home view
 app/views/pages/home.html.erb


	<div class="jumbotron center">
	  <h1>Welcome to my app!</h1>
	  <% if user_signed_in? %>
	       # do something
	  <% else %>
	    <p>
	      <%= link_to "Log in", new_user_session_path, class: "btn btn-default btn-lg" %>
	      <%= link_to "Sign up", new_user_registration_path, class: "btn btn-primary btn-lg" %>
	    </p>
	<% end %>
  
 </div>
 ## 2. Update your header partial
 app/views/layout/_header.html.erb


	<ul class="nav navbar-nav navbar-right">
	         <li><%= link_to "Home", root_path %></li>
	         <li><%= link_to "About", about_path %></li>
	         <% if user_signed_in? %>
	           <li><%= link_to "Log out", destroy_user_session_path, method: :delete %></li>
	         <% else %>
	           <li><%= link_to "Sign in", new_user_session_path %></li>
	         <% end %>
	</ul>

# Enforcing Strong Passwords

Wow — You've just unlocked ANOTHER Bonus Lesson! For your project you've set up Devise to handle passwords, but if you're curious to see what's happening behind the scenes, Jon Rose is here to walk you through how to enforce stronger passwords.

## 1. Add a format validator to the password field
1. Let's add a requirement that all new passwords have some complexity requirements
1. By adding a regular expression to the password field in the user model. A regular expression is a way to check and see if certain characters are present in a string. You can learn more about regular expressions here  
1. In the User model, we want to add a password validator (we didn't have one before) and a :format field for the password:

app/models/user.rb


	validates :password, format:
	    {
	      with:    /\A(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[[:^alnum:]])/,
	      message: "must include at least one lowercase letter, one uppercase letter, and one digit"
	    }

Notice that we don't have the following as they aren't needed:

:confirmation => true // We don't have a password confirmation on our pinteresting app so this does nothing.

:length => // We don't need this because it gets set in the devise initializer at [config/initializer/devise.rb][7465ba36]

  [7465ba36]: https://github.com/onemonthrails/pinteresting/blob/master/config/initializers/devise.rb#L141 "passowrd length"

## 2. Regular expressions

Here is the regular expression we are using:

	/\A.*(?=.{10,})(?=.*\d)(?=.*[a-z])(?=.*[A=Z])(?=.*[\@\#\$\%\^\&\+\=]).*\Z/ //Did not work for me

# 3. Wow - thats complicated!  
1. Let's break this down into smaller pieces so we can understand it...
1. Let's start with the beginning and the end
1. Regular expressions exists between two forward slashes, and anything in between is what we want to match. Check this out:

`/ this is what we match between the forward slashes / `
Next, we want to make sure we match everything in the string, starting from the very beginning all the way to the end, so we include the following two special regular expression characters:

`/\A this is what we match between the forward slashes \Z/`
Everything else in our regular expression is grouped in (). Let's look at each of these groups individually. One thing you will notice is the following construct:

`(?=.*   SOMETHING WE WANT TO MATCH   )`
How this works is that is creates a group which is "looking ahead" in the string to match "SOMETHING WE WANT TO MATCH".  These look forward in the string to identify a pattern, matching anything (.) zero or more times followed by whatever we want to match.  We reuse this over and over in our regular expression to match different things, like numbers, special characters, etc.

First, we match for at least 10 characters:

(?=.{10,})
Next, we match for digits (numbers), using "\d":

(?=.*\d)
Then, we match a lowercase alphabet character:

(?=.*[a-z])
And then we match an uppercase alphabet character:

(?=.*[A-Z])
This code matches a special character.  Note how each special character needs to be escaped using the "\" character:

(?=.*[\@\#\$\%\^\&\+\=])
Now, when we put those together, we ensure passwords are at least 10 characters long, contains a number,  a lowercase and uppercase letter, as well as a special character.
A great site to use to test and learn more about ruby regular expressions is [rubular][1548ebef].
One thing to keep in mind when building regular expressions is that since you probably won't build them all the time, it can be a pain to remember how to do it correctly. I frequently use rubular and examples on the internet as references
# 4. Testing our changes
1. On the homepage click on the ‘sign up’ button
1. Try to sign up with a user with a weak password
1. Click on ‘submit’ and you’ll get the message ‘password is invalid’, meaning our complexity requirements work!
1. It's probably a good idea to update the sign up page to tell users we require strong passwords! Now they know we take security serious! (We cover more on password requirements in the Web Security Class.)

  [1548ebef]: http://rubular.com/ "rubular"

  # Pushing Users to Heroku & Migration

  You should regularly push your changes to Heroku to make sure everything is working in production. Don't forget to rake db:migrate if you made any database changes!

## 1. Do the GIT Dance

	$ git add .
	$ git commit -am "Your Message"
	$ git push
	```
## 2. Push to Heroku
	$ git push heroku master`
Q: How do I debug Heroku?

User the Heroku Logs

	$ heroku logs --tail 
	CONTROL + C` # To exit the Heroku Logs
## 3. Migrate your Heroku database
	$ heroku run rake db:migrate

# Generate Pins Scaffold

By adding a scaffold we can quickly add new pins to our app

## 1. Generate a pins scaffold
	$ rails generate scaffold pins description:string`
	$ rake db:migrate #run the migration`

## 2. Delete the default scaffold CSS
This file gets created with scaffold, and messes up the rest of your CSS, so just delete it

	app/assets/stylesheets/scaffolds.css.scss

Q: Could I tell Rails, "Hey bud, just don't install that ugly default scaffold stylesheet?"

Yes you could:

	$ rails generate scaffold pins description:string --skip-stylesheets

# In this lesson we'll get a better understanding of controller actions

Browse Source Code

Let's clean-up the pins_controller.rb to remove the default stuff we don't need. By removing all that excess, this will also make it easier to understand what's going on in our controller.

app/controllers/pins_controller.rb


	class PinsController < ApplicationController
	  before_action :set_pin, only: [:show, :edit, :update, :destroy]
	
	  def index
	    @pins = Pin.all
	  end
	
	  def show
	  end
	
	  def new
	    @pin = Pin.new
	  end
	
	  def edit
	  end
	
	  def create
	    @pin = Pin.new(pin_params)
	    if @pin.save
	      redirect_to @pin, notice: 'Pin was successfully created.'
	    else
	      render :new
	    end
	  end
	
	  def update
	    if @pin.update(pin_params)
	      redirect_to @pin, notice: 'Pin was successfully updated.'
	    else
	      render :edit
	    end
	  end
	
	  def destroy
	    @pin.destroy
	    redirect_to pins_url
	  end
	
	  private
	    # Use callbacks to share common setup or constraints between actions.
	    def set_pin
	      @pin = Pin.find(params[:id])
	    end
	
	    # Never trust parameters from the scary internet, only allow the white list through.
	    def pin_params
	      params.require(:pin).permit(:description)
	    end
	end





#Let's get a better understanding of Views

 1. Delete these files

We won't be using the JSON files, so we might as well get rid of them

app/views/pins/index.json.jbuilder
app/views/pins/show.json.jbuilder

 2. This is called the "form" partial

apps/views/pins/new.html.erb

    <%= render 'form' %>
    apps/views/pins/_form.html.erb

    <%= form_for(@pin) do |f| %>
      <% if @pin.errors.any? %>
        <div id="error_explanation">
          <h2><%= pluralize(@pin.errors.count, "error") %> prohibited this pin from being saved:</h2>

          <ul>
          <% @pin.errors.full_messages.each do |msg| %>
            <li><%= msg %></li>
          <% end %>
          </ul>
        </div>
      <% end %>

      <div class="form-group">
        <%= f.label :description %>
        <%= f.text_field :description, class: "form-control" %>
      </div>
      <div class="form-group">
        <%= f.submit class: "btn btn-primary" %>
      </div>
    <% end %>

 1. Push your app live

Remember to commit to Github, Heroku and migrate your database on Heroku

    $ heroku run rake db:migrate

Q: I get an error from Heroku: "Failed to push"?

It's possible that Heroku can get out of date from your local changes. To put it another way, your git repo could get ahead or behind Heroku and they want to play it safe with a warning. If you know all is well then run then use the -f bomb to FORCE it

    $ git push -f heroku master #the -f forces the changes to save

Q: What are some additional resources on Git?

[Learn Git Branching Game](http://learngitbranching.js.org/)
[GitHub Guides \[youtube.com\]](https://www.youtube.com/user/GitHubGuides?feature=watch)

# Pins Views

Let's get a better understanding of Views

## 1\. Delete these files

We won't be using the JSON files, so we might as well get rid of them

    app/views/pins/index.json.jbuilder
    app/views/pins/show.json.jbuilder



### 2\. This is called the "form" partial

_apps/views/pins/new.html.erb_

    <%= render 'form' %>

_apps/views/pins/_form.html.erb_

    <%= form_for(@pin) do |f| %>
      <% if @pin.errors.any? %>
        <div id="error_explanation">
          <h2><%= pluralize(@pin.errors.count, "error") %> prohibited this pin from being saved:</h2>

          <ul>
          <% @pin.errors.full_messages.each do |msg| %>
            <li><%= msg %></li>
          <% end %>
          </ul>
        </div>
      <% end %>

      <div class="form-group">
        <%= f.label :description %>
        <%= f.text_field :description, class: "form-control" %>
      </div>
      <div class="form-group">
        <%= f.submit class: "btn btn-primary" %>
      </div>
    <% end %>


### 3\. Push your app live

Remember to commit to Github, Heroku and migrate your database on Heroku

    $ heroku run rake db:migrate

* * *

#### Q: I get an error from Heroku: "Failed to push"?

It's possible that Heroku can get out of date from your local changes. To put it another way, your git repo could get ahead or behind Heroku and they want to play it safe with a warning. If you know all is well then run then use the -f bomb to FORCE it

    $ git push -f heroku master #the -f forces the changes to save


#### Q: What are some additional resources on Git?

*   [Learn Git Branching Game](http://pcottle.github.io/learnGitBranching/)
*   [GitHub Guides [youtube.com]](http://www.youtube.com/user/GitHubGuides?feature=watch)
*   [Try Git [interactive lesson]](http://try.github.io/levels/1/challenges/1)

#Pins Users and Associations

Associations are awesome! It's how you pull this all together and tell Rails that a "User has Pins" in the database


#### Resources

*   Associations: [http://guides.rubyonrails.org/association_basics.html](http://guides.rubyonrails.org/association_basics.html)

### 1\. Set up your associations

#### A Pin belongs_to a User&nbsp;

_app/models/pin.rb_

    class Pin < ActiveRecord::Base
    	belongs_to :user
    end

### 2\. Generate a new migration for a User's index

    $ rails generate migration add_user_id_to_pins user_id:integer:index

### 3\. Start the Rails console 

The Rails console allows us to interact directly with data in the database. You'll do this to update data directly, or mostly just for testing out Ruby code before bringing it into your project

    $ rails console

Once in the console...

    > Pin.connection #This establishes a connection to the database (And spits out a LOT of unnecessary data)
    > Pin.inspect #shows all of the parameters for a Pin 
    > pin = Pin.first #make sure Pin.first is UPPERCASE
    > pin.user

Stop the Console when you're done

    CONTROL + D #closes the Console 

#### A User has_many Pins &nbsp;

_app/models/user.rb_

    class User < ActiveRecord::Base
      # Include default devise modules. Others available are:
      # :token_authenticatable, :confirmable,
      # :lockable, :timeoutable and :omniauthable
      devise :database_authenticatable, :registerable,
             :recoverable, :rememberable, :trackable, :validatable
    
      has_many :pins
    end

### 4\. Let's see if it worked

Back in Rails console (in our terminal) let's set a User ID on a Pin.

    > pin = Pin.first
    > pin #Check out the pin!
    > pin.user_id = 1
    > pin.save
    
    > user = User.first 
    > user.pins
    
    #Now we can call these methods
    > user.pins
    > pin.user

If this doesn't work for you. Try reviewing the video listed in the Troubleshooting section and let us know if that helps!

#Authorization: Who can? Who can't?

In this lesson you'll add authorization (so that a user can't delete another user's pins).

Browse Source Code

#1. Update the Pins Controller


app/controllers/pins_controller.rb

    class PinsController < ApplicationController
      before_action :set_pin, only: [:show, :edit, :update, :destroy]

      def index
        @pins = Pin.all
      end

      def show
      end

      def new
        @pin = current_user.pins.build
      end

      def edit
      end

      def create
        @pin = current_user.pins.build(pin_params)
        if @pin.save
          redirect_to @pin, notice: 'Pin was successfully created.'
        else
          render :new
        end
      end

      def update
        if @pin.update(pin_params)
          redirect_to @pin, notice: 'Pin was successfully updated.'
        else
          render :edit
        end
      end

      def destroy
        @pin.destroy
        redirect_to pins_url
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_pin
          @pin = Pin.find_by(id: params[:id])
        end

        def correct_user
          @pin = current_user.pins.find_by(id: params[:id])
          redirect_to pins_path, notice: "Not authorized to edit this pin" if @pin.nil?
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def pin_params
          params.require(:pin).permit(:description, :image)
        end
    end

2. Update the pins view
-----------------------

app/views/pins/index.html.erb

    <%= pin.user.email if pin.user %>

Or alternatively you could the Ruby "try" method….

    <%= pin.user.try(:email) %>

(I don't choose this one, but it's good to know about)

3. Add devise User authentication
---------------------------------

Resource: https://github.com/plataformatec/devise

Add the before_action to your Pins Controller

app/controllers/pins_controller.rb

    before_action :authenticate_user!, except: [:index, :show]

4. Surround the edit link with an "if" conditional
--------------------------------------------------

This way you can only see your pins. To put that another way: A user can only see his pins (and not other user's pins). Make sense?

app/views/pins/index.html.erb

    ...
    <% if pin.user == current_user %>
      <%= link_to 'Edit', edit_pin_path(pin) %>
      <%= link_to 'Destroy', pin, method: :delete, data: { confirm: 'Are you sure?' } %>
    <% end %>
    ...

app/views/pins/show.html.erb

    ...
    <% if @pin.user == current_user %>
      <%= link_to 'Edit', edit_pin_path(@pin) %>
    <% end %>
    <%= link_to 'Back', pins_path %>
    ...

5. Add correct_user method
--------------------------

Add the before_action to your Pins Controller

app/controllers/pins_controller.rb

    before_action :correct_user, only: [:edit, :update, :destroy]

6. Surround the "New Pin" link with an "if" conditional
-------------------------------------------------------

app/views/pins/index.html.erb

    ...
    <% if user_signed_in? %>
      <%= link_to 'New Pin', new_pin_path %>
    <% end %>
    ...

Bonus! Helpful commands

    $ git add --all
    $ git commit -am "commit message"
    $ git push
    $ git push heroku master
    $ heroku open

#Paperclip ImageMagick Install

ImageMagick is a gem for converting and formatting images. It's a requisite for Paperclip, so let's install it.

Download ImageMagick
Windows
http://www.imagemagick.org/script/binary-releases.php#windows

Mac
http://cactuslab.com/imagemagick/
Q: How do I know if I installed ImageMagick correctly?

Open a new terminal window and run the command:

$ identify
You should get a response including something about ImageMagick's version...

#Image Upload with Paperclip
Paperclip is a gem that allows you to upload images

##1. Install the paperclip gem

https://github.com/thoughtbot/paperclip
/Gemfile

    gem 'paperclip', '~> 4.2'

Then run:

    $ bundle install


/app/models/pin.rb

    class Pin < ActiveRecord::Base
         belongs_to :user
         has_attached_file :image, :styles =>
         { :medium => "300x300>", :thumb => "100x100>" }
    end


    $ rails generate paperclip pin image

Run and check the migration

    $ rake db:migrate
    $ rake db:migrate:status

##3. Restart your server after adding a gem file

    $ ^C
    $ rails server

##4. Edit the pin form
/app/views/pins/_form.html.erb

    <%= form_for @pin, html: { multipart: true } do |f| %>
    .
    .
    .
      <div class="form-group">
        <%= f.label :image %>
        <%= f.file_field :image, class: "form-control" %>
      </div>
    .
    .
    .

##5. Update the Pins Controller for strong parameters
/app/controllers/concerns/pins_controller.rb

    .
    .
    .
        def pin_params
          params.require(:pin).permit(:description, :image)
        end
    .
    .
    .

##6. Update the pins show view
/app/views/pins/show.html.erb

    <%= image_tag @pin.image.url %>
    .
    .
    .

##7. Update the pins index
/app/views/pins/index.html.erb

    <h1>Listing pins</h1>

    <table>
      <thead>
        <tr>
          <th>Image</th>
          <th>Description</th>
          <th>User</th>
          <th></th>
          <th></th>
          <th></th>
        </tr>
      </thead>

      <tbody>
        <% @pins.each do |pin| %>
          <tr>
            <td><%= image_tag pin.image.url(:medium) %></td>
            <td><%= pin.description %></td>
            <td><%= pin.user.email if pin.user %></td>
            <td><%= link_to 'Show', pin_path(pin) %></td>
            <% if pin.user == current_user %>
              <td><%= link_to 'Edit', edit_pin_path(pin) %></td>
              <td><%= link_to 'Destroy', pin, method: :delete, data: { confirm: 'Are you sure?' } %></td>
            <% end %>
          </tr>
        <% end %>
      </tbody>
    </table>

    <br>
    <% if user_signed_in? %>
      <%= link_to 'New Pin', new_pin_path %>
    <% end %>

##8. Delete pins made by non users

    $ rails console
    $ Pin.first
    $ pin = Pin.first
    $ pin.destroy
    $ Pin.first.destroy

##9. Update the pins show view for image size
/app/views/pins/show.html.erb

    <%= image_tag @pin.image.url(:medium) %>
    .
    .
    .

##10. Commit changes

    $ rails console
    $ git add .
    $ git commit -am "Add image upload with Paperclip"
    $ git push
    $ git config --global push.default matching

    #Paperclip to Amazon S3 Images on Heroku


#Paperclip to Amazon S3 Images on Heroku

###Heroku doesn't store images, so you need AWS for that


#### Resources

*   Paperclip Documentation:&nbsp;[https://devcenter.heroku.com/articles/paperclip-s3](https://devcenter.heroku.com/articles/paperclip-s3)

#### 1\. Add the Amazon AWS Gem

_/Gemfile_

    gem 'aws-sdk', '< 2.0'

Note:&nbsp;This is different from the video! So far you've seen us use **gem 'blah', '~> 3.1'** but we want to make sure we use the newest version of the aws-sdk gem BELOW the 2.0 release. This is because major updates (like moving from 1.x to 2.0) can break specific functionality we're using, and it's a pain to go back and fix.&nbsp;

And run bundle install:

    $ bundle install


### 2\. Add S3 credential placeholders

_config/environments/production.rb_



    config.paperclip_defaults = {
      :storage => :s3,
      :s3_credentials => {
        :bucket => ENV['S3_BUCKET_NAME'],
        :access_key_id => ENV['AWS_ACCESS_KEY_ID'],
        :secret_access_key => ENV['AWS_SECRET_ACCESS_KEY']
      }
    }

Note: The video uses "AWS_BUCKET" for the first variable. This has since been changed to "S3_BUCKET_NAME" so make sure you use the right one. We have more info in the Troubleshooting tab about the error you'll see if you do it wrong.

### 3\. Sign up for an AWS account &nbsp;

[http://aws.amazon.com/](http://aws.amazon.com/)

This may want a credit card number for you, but it does have a free version. If you don't have a credit card or don't feel comfortable giving them your information, you can use a prepaid credit card, or ask your credit card company to issue you a disposable CC number.

### 4\. Create an S3 bucket in AWS &nbsp;

Make sure you use the "US Standard" region when creating your bucket, or you will get errors.

### 5\. Grant permission to everyone &nbsp;

Actions -> Permissions

### 6\. Configure Heroku for AWS &nbsp;

    $ heroku config
    $ heroku config:set AWS_BUCKET=pinteresting
    $ heroku config
    $ heroku config:set AWS_ACCESS_KEY_ID=***GET FROM AMAZON AWS***
    $ heroku config:set AWS_SECRET_ACCESS_KEY=***GET FROM AMAZON AWS***
    $ heroku config

### 7\. Add, commit and push to Git and Heroku&nbsp;

    $ git add --all
    $ git commit -am "Add Amazon S3 for Paperclip uploads to Heroku"
    $ git push
    $ git push heroku master
    $ heroku open
    
#Styling Our App with jQuery Masonry
###1. Add the Masonry gem

	gem 'masonry-rails', '~&gt; 0.2.0'
	
###2. Always bundle install to install a new gem

	$ bundle install
	
###3. Modify our application.js file
	
	.
	.
	.
	//= require masonry/jquery.masonry
	//= require masonry/jquery.imagesloaded.min
	.
	.
	.
	
What is jquery.imagesloaded.min? It's a jQuery plugin that triggers a callback after all images have been loaded, which Masonry likes to use.

###4. Modify our application.css file 
 
/app/assets/stylesheets/application.css

	.
	.
	.
	 *= require 'masonry/transitions'
	.
	.
	.
	
###5. Update our Pins Index  
/app/views/pins/index.html.erb

	<div id="pins">
	  <% @pins.each do |pin| %>
	    <div class="box">
	      <%= image_tag pin.image.url(:medium) %>
	      <%= pin.description %>
	      <%= pin.user.email if pin.user %>
	      <%= link_to 'Show', pin_path(pin) %>
	      <% if pin.user == current_user %>
	        <%= link_to 'Edit', edit_pin_path(pin) %>
	        <%= link_to 'Destroy', pin, method: :delete, data: { confirm: 'Are you sure?' } %>
	      <% end %>
	    </div>
	  <% end %>
	</div>
	
###6. Adding Pins directly from the header  
/app/views/layouts/_header.html.erb

	.
	.
	.
	     <li><%= link_to 'New Pin', new_pin_path %></li>
	.
	.
	.
###7. Update our Pins stylesheet for Masonry CSS  
/app/assets/stylesheets/pins.css.scss

	.
	.
	.
	#pins {
	  margin: 0 auto;
	}
	
	.box {
	  margin: 5px;
	  width: 214px;
	}
	
	.box img {
	  width: 100%;
	}

###7. Update our Pins JavaScript for Masonry  
/app/assets/javascripts/pins.js.coffee

	.
	.
	.
	$ ->
	  $('#pins').imagesLoaded ->
	    $('#pins').masonry
	      itemSelector: '.box'
	      isFitWidth: true
	      
###8. Add CoffeeScript syntax highlighting to Sublime Text  
Command Shift P
Package Control:Install Package
CoffeeScript